\documentclass[aip,pop,preprint,linenumber,superscriptaddress,amsmath,amsfonts]{revtex4-1}
\usepackage{color,graphicx} 
%\usepackage{graphics}
\usepackage{multirow}
%please change to your own drive: pdftex or dvips
%\usepackage[dvips]{color,graphicx}
%\usepackage[pdftex]{color,graphicx}
\usepackage{ifthen}
\usepackage{ulem} % for \sout{} command to "strikethrough" text
\usepackage{bm}

\usepackage[colorlinks,linkcolor=blue,citecolor=blue,bookmarks,pdfstartview=FitH]{hyperref}
%opening

% ### For proposing changes highlighted in blue or raising comments in red ###
\newcommand{\matthias}[1]{{\color{blue}#1}}
\newcommand{\myfigdir}[1]{./figure0/}
\newcommand{\revcolor}[1]{{\color{green}#1}}
\newcommand{\revcolora}[1]{{\color{red}#1}}
\newcommand{\revcolorb}[1]{{\color{blue}#1}}
% ############################################################################

\begin{document}
	
	\title{$p_\parallel$ Formalism}
	\author{G. Meng}
	\affiliation{Max-Planck-Institut f\"ur Plasmaphysik,  85748 Garching, Germany}
	
	%	\date{14.01.2021}
	\maketitle


\section{Shear Alfv\'en wave in uniform slab geometry}
	The gyrokinetic Poisson-Amp\'ere system is described as follows,
	\begin{eqnarray}
	-\nabla_\perp\cdot\sum_s\frac{n_{0s}e_s}{\omega_{cs}B}\nabla_\perp\delta\phi & = & \sum_s e_s\delta n_s \;\;, \\
	-\nabla^2_\perp\delta A_\parallel & = & \mu_0 \sum_s\delta j_{\parallel,s} \;\;,
	\end{eqnarray}
	where $\omega_{cs}=e_s B/m_s$, the subscript `$s$' and `$\parallel$' indicate the species `$s$' and the component parallel to the equilibrium magnetic field respectively, and $\mu_0$ is the vacuum permeability. 
	In the minimum model of SAW, the ion response is described with the polarization density, and only one kinetic species (electron) is kept, 
		\begin{eqnarray}
	-\nabla_\perp\cdot \frac{n_{0i}e_i}{\omega_{ci}B}\nabla_\perp\delta\phi & = & e_e\delta n_e \;\;, \\
	-\nabla^2_\perp\delta A_\parallel & = & \mu_0 \delta j_{\parallel,e} \;\;.
	\end{eqnarray}
	The guiding center's equations of motion are as follows,
	\begin{eqnarray}
	\frac{dl}{dt} &=& v_\parallel\;\;,\\
	\frac{dv_\parallel}{dt} &=& -\frac{e_s}{m_s} \left( \partial_\parallel\delta\phi+\partial_t\delta A_\parallel \right) \;\;,
	\end{eqnarray}
	where $l$ is the coordinate along the magnetic field.
	\section{Normalization}
	The normalization units of the length, time and charge are $R_N=1\;\text{m}$, 
	$t_N=R_N/v_{te}$ and $e$, where electron thermal velocity $v_{te}=\sqrt{2T_e/m_e}$, $m_e$ is the electron mass, $T_e$ is the electron temperature, $e$ is the elementary positive charge.
	For uniform system, we have $n_{0i}=n_{0e}=n_0$, $T_i=T_e$. 
	The normalization units of the electrical potential, magnetic potential, density and current are $m_e v_{te}^2/e$, $m_e v_{te}/e$, $n_0$, and $-ev_{te}n_0$, respectively.
	Other variables are normalized using $v_{te}$, $t_N \ldots$, i.e., $v_\parallel=\bar{v}_\parallel v_{te}$, $l=\bar{l}R_N$. In the following, for the sake of simplicity, the bar  $\bar{\ldots}$ is omitted when no confusion is introduced. 
	\begin{table}[htbp]\centering 
		\caption{\label{tab:norm_unit} The normalization units.}
		\footnotesize
		\begin{tabular}{l|c|c}
			  Name     &  Notation  &   Unit \\ \hline
			  Length   & $l$   &  $R_N=1\;\text{m}$   \\
			  Time     & $t$   & $t_N=R_N/v_{te}$ \\ 
			  Mass     & $m$   & $m_e$ \\
			  Charge   & $e_s$ &  $e$ \\ 
			  Density  & $\delta n_s$ & $n_0$  \\ \hline
			  Velocity & $v$    & $v_{te}$ \\
			  Electrical potential & $\delta\phi$ &  $m_e v_{te}^2/e$ \\
			  Magnetic potential &  $\delta A$ & $m_e v_{te}/e$ \\
			  Current  & $\delta j$ & $-en_0v_{te}$ 
		\end{tabular}
	\end{table}
	Noticing that $\bar{e}_s=-1$ for $s=e$, the normalized equations are
	
	\begin{eqnarray}
	\label{eq:dldt1d}
	\frac{dl}{dt} &=& v_\parallel\;\;,\\
	\label{eq:dvdt1d}
	\frac{dv_\parallel}{dt} &=&  \partial_\parallel\delta\phi+\partial_t\delta A_\parallel  \;\;, \\
	\label{eq:poisson1d}
	\nabla_\perp^2\delta\phi & = & C_P\delta N \;\;, \\
	\label{eq:ampere1d}
	\nabla^2_\perp\delta A_\parallel & = & C_A\delta J_\parallel \;\;,
	\end{eqnarray}
	where the physical factors $C_P  = \frac{1}{\bar{e_i}^2}{1}/\rho_{ti}^2$,
	$C_A  = \frac{1}{\bar{e_i}^2}\frac{m_i}{m_e} \; {\beta}/{\rho_{ti}^2} $, ${\rho_{ti}}=m_iv_{ti}/e_iB$, $\beta=2\mu_0 n_0 T_e/B^2$.
\section{$p_\parallel$ formalism}
%The normalized Eqs. \ref{eq:dldt1d} to \ref{eq:ampere1d} are in the $v_\parallel$ formalism. 
Define $p_\parallel\equiv v_\parallel-\delta A_\parallel$ and replace $v_\parallel$ in the Eq. \ref{eq:ampere1d},
\begin{equation}
	dp_\parallel/dt=\partial_l \delta \phi-(p_\parallel+\delta A_\parallel) \partial_l \delta A_\parallel.
\end{equation} 
	
The Fourier components of the density and current are obtained using particle-in-Fourier in the parallel direction,
	\begin{eqnarray}
	\label{eq:deltaNk}
	\delta{N}_{k_l} &=& \frac{1}{N_{ptot}}\sum_p e^{-ik_ll_p} \;\;, \\
	\label{eq:deltaJk}
	\delta{J}_{k_l} &=& \frac{1}{N_{ptot}}\sum_p v_{\parallel,p}e^{-ik_l l_p} \;\;,
	\end{eqnarray}
	where the Fourier decomposition is applied to the field and moment variables, e.g, $\delta N(l)=\sum_k \delta N_{k_l } \exp\{i{k_l}l\}$, and $k_l$ is the wave vector along $l$. 
	
	Submit Eq. \ref{eq:deltaJk} to Eq. \ref{eq:ampere1d},
	\begin{equation}
		\nabla_\perp^2 \delta A_k= \frac{C_A}{N_{ptot}} \sum_p (p_\parallel+\delta A) e^{-i k_l l_p}
	\end{equation}
    we have
    \begin{eqnarray}
    \label{eqp:dldt }
    \frac{dl}{dt} &=& p_\parallel+\delta A\;\;,\\
    \label{eqp:dpdtp}
    \frac{dp_\parallel}{dt} &=&  \partial_l\delta\phi-(p_\parallel+\delta A)\partial_l \delta A  \;\;, \\
    \label{eqp:poissonp}
    \delta\phi_k & = & -\frac{C_P}{k_\perp^2} \frac{1}{N_{ptot}} \sum_p e^{-i k_l l_p} \;\;, \\
    \label{eqp:amperep}
    \delta A_k & = & -\frac{ C_A}{k_\perp^2+C_A} \frac{1}{N_{ptot}} \sum_p p_{\parallel,p} e^{-i k_l l_p} \;\;.
    \end{eqnarray}	
    
	
	
		%\bibliographystyle{unsrt}
	%\bibliography{references}
	
\end{document} 
	