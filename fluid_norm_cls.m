%=======================A pure fluid test code=======================
% memo:
% 2021/2/3: simple 2 equations by ZLU
%
% Equations:
%   dEx/dt= Bx  (1)
%   dBx/dt=-Ex  (2)
% analytical dispersion relation: d^2 E/dt^2+E=0, i.e., w^2=1

classdef fluid_norm_cls
    properties
        dt
        Exk
        Byk
        kx
        kz
        cor
        Density
        Pressure
        Current
        PIFfac
        parms
        norm
        sfile
        xwid
        ywid
        zwid
        xleft
        yleft
        zleft
        N_mark
        N_particles
        N_PIF_modes
    end % PROPERTIES
    methods
        function s = fluid_norm_cls
            %Set initial values for the field
            s.parms.B0 = 5000; % Gauss non-normalized. 5000 makes the normalized E  field on the order of 1. Hardcoded on the order of 1. Maybe define in terms of beta, Dr. Lu?
            Byk_init = 0.05 * s.parms.B0; % Gauss. non-normalized
            Exk_init = 1e-7 * Byk_init; % E in cgs units. non-normalized %Too small?
            % Set PIF parameters
            s.N_mark = 10e3;
            s.N_particles = s.N_mark * 10e10;
            s.parms.n0 = 1e13;
            s.N_PIF_modes = 2;
            s.PIFfac = 20;

            % Set plasma parameters
            s.parms.R_N = 100; %cm
            s.parms.beta = 0.01;
            s.parms.miome = 1836;
            s.parms.c = 29979245800; %cm/s, cgs
            s.parms.kb = 1.3807e-16; % cm^2 g / (s^2 K)
            s.parms.e = 4.8e-10; % elementary charge in cgs
            s.parms.me = 9.10938370e-28; % g
            s.parms.mi = s.parms.me * s.parms.miome; % g of protons
            s.parms.T = s.parms.B0^2 * s.parms.beta / (8*pi*s.parms.n0*s.parms.kb);
            s.parms.T_e = s.parms.T; % K, cold electrons
            s.parms.T_i = s.parms.T;% K, warm ions
            % Calculate plasma parameters
            s.parms.v_te = sqrt(2 * s.parms.kb * s.parms.T_e / s.parms.me); % cm/s.
            s.parms.v_ti = sqrt(2 * s.parms.kb * s.parms.T_i / s.parms.mi); % cm/s.
            s.parms.C_E = s.parms.me * s.parms.v_te^2 / (s.parms.e * s.parms.R_N); % E in cgs. constant to normalize electric field
            s.parms.C_B = s.parms.me * s.parms.v_te / (s.parms.e * s.parms.R_N) * s.parms.c; % c*B in cgs. constant to normalize magnetic field
            s.parms.rho_ti = s.parms.c *s.parms.mi * s.parms.v_ti / s.parms.e / s.parms.B0; % cgs thermal ion larmor radius

            % Calculate wave numbers
            s.kx = (1/s.parms.rho_ti * 0.4); %1/cm. The decimal here is k_x rho_ti
            s.kz = s.kx/10; % 1/cm
            % Define the geometry and calculate the Alfven frequency
            s.zleft = 0;
            s.zwid = 2*pi/s.kz; % cm. Enforces periodic boundary condition. 1 wavelength
            s.parms.rho_m = s.parms.n0 * (s.parms.me + s.parms.mi); % g/cm mass density
            s.parms.v_A =  s.parms.B0 / sqrt(4*pi*s.parms.rho_m);
            s.parms.omega_A = s.parms.v_A * abs(s.kz);
           
            % Define some normalized variables
            s.norm.Exk = Exk_init / s.parms.C_E; 
            s.norm.Byk = Byk_init / s.parms.C_B;
            s.norm.kx = s.kx * s.parms.R_N;
            s.norm.kz = s.kz * s.parms.R_N;
            s.norm.rho_ti = s.parms.rho_ti / s.parms.R_N ;
            s.norm.omega_A = s.parms.omega_A * s.parms.R_N / s.parms.v_te;
            s.norm.T_A = 2*pi/s.norm.omega_A;
            s.norm.zwid = s.zwid / s.parms.R_N;
            s.norm.zleft = s.zleft / s.parms.R_N;            
            
            % Find initial perturbation
            errDens = 1/sqrt(s.N_mark);
            pertDens_o_err = 2;
            pert = errDens * pertDens_o_err * 2/s.norm.kz; %Normalized
            % Initialize particles
            s.cor(1,:) = (rand(s.N_mark,1).*s.zwid+s.zleft)/s.parms.R_N; % Normalized
            s.cor(1,:) = s.cor(1,:)+ pert.*sin(s.kz*s.cor(1,:)); 
%             s.cor(2,:) = zeros(1,s.N_mark);
            s.cor(2,:) = normrnd(0,1/sqrt(2),s.N_mark,1); % Normalized by construction
            % Initialize field variables
            s.Density  = zeros(1,s.N_mark);
            s.Pressure = zeros(1,s.N_mark);
            s.Current = zeros(1,2);
         end
        
        function s = test_fluid(s,tend,dt)
            if~exist('tend','var');tend = pi;end
            if~exist('dt','var');dt = 0.05;end
            % Normalize times
            dt = dt *  s.norm.T_A; % Defined in fractions of a normalized Alfven period
            tend = tend * s.norm.T_A;
            nrun = floor(tend/dt);
            s.dt = dt;
            
            s = s.record([],[],0); %saving
            for fic = 1:nrun
                s = s.onestep_rk4(s.dt); %compute the next field & particles
                s.record(s.norm.Exk,s.norm.Byk,fic,s.sfile);%save result
%                 s = s.setBC;
%                 s = s.calc_Density;
%                 s.plotstep(fic)
                if rem(fic,10)==0; disp([sprintf('%3.1f',fic/nrun*100),'% complete']); end 
            end
            s.plot_end; % plot the end results
        end
                
        function s = onestep_euler(s,dt)
            var = s.get_dfdt(s.norm.Exk, s.norm.Byk);
            s.norm.Exk = s.norm.Exk + var.dExkdt * dt;
            s.norm.Byk = s.norm.Byk + var.dBykdt * dt;
            s.cor(1,:) = s.cor(1,:) + s.cor(2,:) * dt;
            s.cor(2,:) = s.cor(2,:) + var.dvzdt .* dt;           
        end
        
        function s = onestep_rk4(s,dt)
            dthalf = dt/2;
            E = s.norm.Exk;
            B = s.norm.Byk;
            % Calculate k1
            E1 = E;
            B1 = B;
            k1 = s.get_dfdt(E1,B1);
            % Calculate k2
            E2 = E + dthalf * k1.dExkdt;
            B2 = B + dthalf * k1.dBykdt;
            k2 = s.get_dfdt(E2,B2);
            % Calculate k3
            E3 = E + dthalf * k2.dExkdt;
            B3 = B + dthalf * k2.dBykdt;
            k3 = s.get_dfdt(E3,B3);
            % Calculate k4
            E4 = E + dt * k3.dExkdt;
            B4 = B + dt * k3.dBykdt;
            k4 = s.get_dfdt(E4,B4);
            % Determine next position
            s.norm.Exk = E + dthalf/3 * (k1.dExkdt + 2*k2.dExkdt + 2*k3.dExkdt + k4.dExkdt);
            s.norm.Byk = B + dthalf/3 * (k1.dBykdt + 2*k2.dBykdt + 2*k3.dBykdt + k4.dBykdt);
            % Push particles
            s.cor(1,:) = s.cor(1,:) + s.cor(2,:) * dt;
            s.cor(2,:) = s.cor(2,:) + dthalf/3 * (k1.dvzdt + 2*k2.dvzdt + 2*k3.dvzdt + k4.dvzdt);  
        end
        
        function var=get_dfdt(s,norm_Exk,norm_Byk)
            p = s.parms;
            var.dExkdt = -1i/p.beta * (p.T_i/p.T_e/p.miome) * s.norm.kz * norm_Byk * (1 - 3/8*(s.norm.kx*s.norm.rho_ti)^2)^-1;
            norm_Ezk = s.norm.kz/s.norm.kx * norm_Exk * (1 + p.beta*p.miome / (s.norm.kx*s.norm.rho_ti)^2)^-1;
            var.dBykdt = -1i * (s.norm.kz*norm_Exk - s.norm.kx*norm_Ezk);
            var.dvzdt = - norm_Ezk;
        end
        
        function s = record(s,normExk,normByk,irun,sfile)
            if ~exist('sfile','var')
                sfile = 'data_field.nc';
                s.sfile = sfile;
            end
            if(irun == 0)
                if exist(sfile,'file') == 2
                    delete(sfile);
                end
                nccreate(sfile,'Exk','Dimensions',{'t',Inf});
                nccreate(sfile,'Byk','Dimensions',{'t',Inf});
                nccreate(sfile,'dt');
                ncwrite(sfile,'dt',s.dt);
            elseif(irun > 0)
                ncwrite(sfile,'Exk',real(normExk),irun)
                ncwrite(sfile,'Byk',real(normByk),irun)
            end
        end

  
        function s = setBC(s)
            left = s.norm.zleft;
            right = s.norm.zleft + s.norm.zwid;
            s.cor(1,s.cor(1,:) > right) = s.cor(1,s.cor(1,:)> right) - s.norm.zwid;
            s.cor(1,s.cor(1,:) < left) = s.cor(1,s.cor(1,:) < left) + s.norm.zwid;
        end
        
        function s = calc_Density(s)
            z = s.cor(1,:);
            k_vec = 2*pi./s.norm.zwid .* (0:s.N_PIF_modes-1);
           
            % Calculate Fourrier components
            k_rep1 = repmat(k_vec,length(z),1); 
            y_rep = repmat(z',1,s.N_PIF_modes);
            n_k = s.parms.n0/s.N_mark * sum(exp(-1i.*k_rep1.*y_rep),1);
            
            % Compute density in configuration space
            z_cors = linspace(s.norm.zleft,s.norm.zleft+s.norm.zwid,s.N_PIF_modes*s.PIFfac);
            k_rep2 = repmat(k_vec,s.N_PIF_modes*s.PIFfac,1);
            z_cors_rep = repmat(z_cors',1,s.N_PIF_modes);
            s.Density = 2*real(sum( n_k .* exp(1i.*k_rep2.*z_cors_rep),2))-real(n_k(1));
        end
        
        function plotstep(s,fic)
            figure(1)
            clf;
            hold on
            subplot(1,2,1)
            y = (real(s.cor(1,:)));
            plot(y,'b.')
            ylabel('z')
            xlabel('Particle index')
            ylim([0,2*pi/s.norm.kz])
            title(['Particles at step ',num2str(fic)])
            
            subplot(1,2,2)
            x = s.Density;
            y = linspace(s.norm.zleft,s.norm.zleft+s.norm.zwid,s.N_PIF_modes*s.PIFfac);
            plot(x,y,'r')
            ylim([0,2*pi/s.norm.kz])
            xlim([s.parms.n0*.95,s.parms.n0*1.05])
            title('Density')
            xlabel('Number density')
            ylabel('z')
            pause(0.000001)
            hold off
            
            figure(2)
            clf;
            x = real(s.cor(1,:));
            y = real(s.cor(2,:));
            plot(x,y,'b.')
            xlabel('z')
            ylabel('v_z')
            title('Dependence of v_z on z')
            xlim([0,2*pi/s.norm.kz])
        end
        
        function var = plot_end(s)
            var.Exk = ncread(s.sfile,'Exk')* s.parms.C_E;
            var.Byk = ncread(s.sfile,'Byk')* s.parms.C_B;
            var.dt = ncread(s.sfile,'dt');
            
            nrun = length(var.Exk);
            t1d =(1:nrun) * s.dt;
            
            T_Exk = s.calc_period(var.Exk,t1d);
            omega_Exk = 2*pi / T_Exk;
            T_Byk = s.calc_period(var.Byk,t1d);
            omega_Byk = 2*pi / T_Byk;
            
            omega = mean([omega_Exk omega_Byk]);
            disp('Compare')
            disp('Numerical')
            disp('omega / omega_A /k_z')
            disp(omega/s.norm.omega_A)
            
            fdisp = @(x) sqrt( (1 + 1./(1+1./(x.^2/s.parms.beta/s.parms.miome))) ./ (1- 3/8*x.^2) );
            omega_anal = fdisp(s.norm.kx*s.norm.rho_ti);
            disp('Analytic')
            disp(omega_anal)
            
            disp('Error')
            disp((omega/s.norm.omega_A - omega_anal)/omega_anal)
            
            figure('position',[50 50 500 400])
            plot(t1d,abs(var.Exk),'r',t1d,abs(var.Byk),'k')
            set(gca, 'YScale', 'log')
            lid = legend(strcat('Ex, T=',sprintf('%2.3f',T_Exk)),strcat('By, T=',sprintf('%2.3f',T_Byk)));
            set(lid,'location','best','box','off')
           
            %title(['theory T=', num2str(s.parms_aux.T)])
            xlabel('Time, s')
            ylabel('Field strength')
            grid;
            grid minor;
        end
        
        function T = calc_period(~,y,x)
            try
                [pk,locs] = findpeaks(abs(y),x);
                [~,locs_diff] = findpeaks(abs(diff(y)),x(1:end-1));
                per = mean(diff(sort([0,locs, locs_diff])))*4; % Estimate period from zeros and peaks
                if isnan(per)
                    per = max(x)*4;
                    disp('Error: not enough oscilations')
                end
                if (y(1)>(y(2)-y(1)))
                    pk = [y(1);pk];
                end
                decay = 0;%pk(end) * log(pk(end)/pk(1));
                amp = (pk(1)+pk(2))/2;
                offset = 0;                               % Estimate offset
                fit = @(b,x)  b(1).*exp(b(5)*x).*(sin(2*pi*x./b(2) + b(3))) + b(4);    % Function to fit
                options = optimoptions('lsqcurvefit','OptimalityTolerance',1e-30,'FunctionTolerance',1e-30,'Display','off');
            catch
                T = 0;
            end
            try
                sval = lsqcurvefit(fit,[amp,per,pi/2,offset,decay],x,y',[],[],options);
                T = sval(2);
                xp = linspace(min(x),max(x)); figure(); plot(x,y,'b.-',  xp,fit(sval,xp), 'r-')
                grid; title('Least squares fit function'); legend('data','fit');
            catch
                T = per;
            end
        end
    end
end


