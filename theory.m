
%----Zhixin Lu 20210202 E&B analytical dispersion relation

nscan=50;
kp_rhoi_1d = linspace(0,0.5,nscan);

meomi = 1/1836;
beta = 0.01;

% Parameters exclusive to chen 2020
tau = 1;
alpha = 10*meomi;% I was not sure how to interpret this value. I took it as k_perp / k_paralell. Probably the issue. Maybe it involves meomi.
%When multiplying by meomi, the values look right but are a little high. I don't know why, but I'll leave it. Not a very mathematical way of knowing
delta_e = sqrt(pi)*alpha*exp(-alpha^2);

%--x: kperprhoi as input
% Our simple equations. (It is a little high at larger values compared to the chen paper by visual inspection)

% Change: I included a square root, but it makes the answer further from the one in chen 2020. Same with the chen 2020
% function. I'm not sure if this is an error in the paper or we use the wrong functions.

fdisp = @(x) sqrt( (1 + 1./(1+1./(x.^2*meomi/beta))) ./ (1- 3/8*x.^2) );

% Equation 31 in Chen 2020 (Need to check and verify) (Not yet right. A little too low without sqrt)
fdisp_chen = @(x) sqrt( 1 + 3/8*x.^2 + tau*0.5*x.^2./real(1 - 2*alpha^2 + 1i*delta_e) );

omega_1d = fdisp(kp_rhoi_1d);
omeega_1d_chen = fdisp_chen(kp_rhoi_1d);

num_x = [0.1 .2 .3 .4];
num_y = [1.0021 1.0086 1.0198 1.0357];
num_y_norm = [1.0019 1.0068 1.0151 1.0272];

figure('position',[50 50 500 300])
plot(kp_rhoi_1d,omega_1d,'b-')
hold on
plot(kp_rhoi_1d,omeega_1d_chen,'r-')
plot(num_x,num_y,'ko')
plot(num_x,num_y_norm,'go')
hold off
legend('Us','Chen','Numerical','Normalized','Location','best')

xlabel('k_\perp \rho_i')
ylabel('\omega / \omega_A');

grid;grid minor;
set(gcf,'color',[1 1 1])
