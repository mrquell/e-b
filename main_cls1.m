%=================The E&B model 2021===================
% Maxwell, Zhixin
% simple test:
% >>emnew=main_cls1;emnew1=emnew.test_em

classdef main_cls1
    properties
        %--geometry------------------
        yleft=0; % left boundary along x
        ywid=1;  % width along x
        nk=1;    % wave number, note kx=2*pi*nk/xwid;
        %------------------
        yright=[]; % xright will be calculated using xleft and xwid
        kx=[];     % kx=2*pi*nk/xwid, calculated in gkem1d_cls3
        kz=[];
        %--particle------------------
        me=9.10938e-31*1e3; %--but me only enters field (Ampere) equation; electron mass in g. CGS units
        mi = 1.6726219e-27*1e3; %Proton mass in g CGS units
        nptot=1e6/10*4;% electron marker number. Number of total particles
        Npart = 1e10;
        n_dens = Npart/ywid;
        pert_dens_o_min=2; % it gives the amplitude of the initial density perturbation, normalized to the noise (or minimum) level (~1/sqrt(nptot))
        pertdens;
        perty=[]; % the perturbation in particle coordinate y (for initialization)
        err_dens_min; %the noise level (~1/sqrt(nptot))
        vmaxovte=6; % the max velocity devided by the thermal electron velocity (only for diagnosis)
        %------------------
        party=[]; % particle coordinate "y"
        partvy=[]; % particle velocity "v_y"
        %--field parameters------------------
        %kperp=20; % perpendicular wave vector
        rhot=0.02; % Ion larmor radius
        beta=0.01   ; % plasma beta
        B0 = 1; % Background magnetic field
        del_Exk;
        del_Ezk;
        delt_Byk;
                
        %------------------
        densk,jpark % Fourier component of density and parallel current perturbation
        phik % Fourier component of delta phi
        apark % Fourier component of delta A||
        Cpoisson % the coefficient C_P in the Poisson Equation
        Campere  % the coefficient C_A in the Ampere Equation
        %--Time Parameters
        t0;  % starting time of simulation
        t1;  % final time of simulation
        dt;  % time step size
        %--------parameters & data----------
        parms_aux=[]; % auxiliary variables that will be calculated in the gkem1d_cls3
        data_energy=[];
    end
    
    methods
        function this=gkem1d_cls3(parms)
            %--return this: the object of gkem1d_cls with particle
            %variables and other physics & geometry data
            if(~exist('parms','var'));parms=[];disp('====Using default parms====');
            else;disp('====Using parms from argument====');end
            %--geometry
            if (isfield(parms,'xleft'));    this.xleft=parms.xleft;     end
            if (isfield(parms,'xwid'));     this.xwid=parms.xwid;       end
            if (isfield(parms,'nk'));       this.nk=parms.nk;           end
            %--particle
            if (isfield(parms,'me'));       this.me=parms.me;           end
            if (isfield(parms,'nptot'));    this.nptot=parms.nptot;     end
            if (isfield(parms,'pert_dens_o_min'));this.pert_dens_o_min=parms.pert_dens_o_min;end
            if (isfield(parms,'vmaxovte')); this.vmaxovte=parms.vmaxovte;end
            %--field parameters
            if (isfield(parms,'kperp'));    this.kperp=parms.kperp;     end
            if (isfield(parms,'rhot'));     this.rhot=parms.rhot;       end
            if (isfield(parms,'beta'));     this.beta=parms.beta;       end
            
            %--Calculate induced variables--
            this.yright=this.yleft+this.ywid;
            this.kx=2*pi*this.nk/this.ywid;
            this.kz=this.ky;
            %--Perturbation
            this.err_dens_min=1/sqrt(this.nptot); % noise level
            this.pertdens=this.err_dens_min*this.pert_dens_o_min;
            
            this.perty=-2/this.kx*this.pertdens;
            %
            this.Cpoisson=1/this.rhot^2;
            this.Campere=this.beta/(this.rhot^2*this.me);
            
            this.parms_aux.wa=this.kx*sqrt(this.Cpoisson/this.Campere); %Alfven frequency
            this.parms_aux.Ta=2*pi/this.parms_aux.wa; %Alfen period normalized to w_{te}
            this.parms_aux.vphase=sqrt(this.Cpoisson/this.Campere); %Wave phase velocity
            %--need to check--
            %this.parms_aux.pe=sqrt(this.Cpoisson)*this.kx/this.kperp; % plasma frequency
            %this.parms_aux.Tpe=2*pi/this.parms_aux.pe; % period
            %
            this.showinfo
            %--init particles--
            this.party=rand(this.nptot,1)*this.ywid+this.yleft; % uniform particle initialization along x
            this.party=this.party+this.perty*sin(this.kx*this.party); % add initial perturbation
            
            this.partvy=normrnd(0,1/sqrt(2),this.nptot,1); % Maxwell distribution along v
            %--calc. consistent field--
            this.del_Exk = 0;
            this.del_Ezk = 0;
            this.delt_Byk = 0;
            %this.phik=this.poisson_solve(this.party); % set the initial field phik
            %this.apark=this.ampere_solve(this.partx,this.partv); % set the initial field apark
            %--
        end %--GKEM1d_CLS1
        
        
        function showinfo(this)
            % show the info of "this" (object of gkem1d_cls)
            % use "this.showinfo", where "this" is the object you declared
            disp([' geometry: xleft=',num2str(this.xleft),',xwid=',num2str(this.xwid),',nk=',num2str(this.nk)]);
            disp([' particle: me=',num2str(this.me),',np=',num2str(this.nptot),',pertx=',num2str(this.pertx),',vmaxovte=',num2str(this.vmaxovte)]);
            disp([' field: kperp=',num2str(this.kperp),',rhot=',num2str(this.rhot),',beta=',num2str(this.beta)]);
            disp(['  beta/me=',num2str(this.beta/this.me)])
            disp(['** Pert. N/Err(N)=',num2str(this.pertdens/this.err_dens_min),', Err(N)=',num2str(this.err_dens_min),',pertN=',num2str(this.pertdens)])
        end
        
        function [Exk,Ezk,dBdt] = fieldsolve(this)
            c = 3e8;
            qe = -1.60217662e-19;
            rho_ti = 1;%????? How do you define this?
            U_yk = ((this.Npart)/(this.ywid*this.nptot))*sum(this.partvy.*exp(-li*this.kx*this.party));
            this.del_Exk = (1/c).*U_yk.*this.B0./(1-(this.kx.^2 * this.n_dens * this.mi * rho_ti));
            this.del_Ezk = - this.kz/this.kx * this.del_Exk * (1+(4*pi)/(c^2)*(qe^2)/this.me * n0);
            this.delt_Byk = -li*c*(this.kz.*this.del_Exk - this.kx.*this.del_Ezk);
            Exk = this.del_Exk;
            Ezk = this.del_Ezk;
            dBdt = this.delt_Byk; 
        end
        
%         function var=poisson_solve(this,partx)
%             %--solve poisson equation using partx
%             %--return var: phik solved from partx
%             ftmp=-this.Cpoisson/(this.kperp^2*this.nptot); %document.pdf eqn 17
%             var=ftmp*sum(exp(-1i*this.kx*partx)); %document.pdf eqn 17
%         end
%         
%         function var=ampere_solve(this,partx,partv)
%             %--solve Ampere equation using partx, partv
%             %--return var: apark solved from partx,partv
%             ftmp=-this.Campere/((this.kperp^2+this.Campere)*this.nptot); % document.pdf eqn 18
%             var=ftmp*sum(partv.*exp(-1i*this.kx*partx)); % document.pdf eqn 18
%         end
        
        
        
        function [xx,vv]=addxv(~,partx0,partv0,fac1,partx1,partv1,fac2)
            % summation operation
            xx=partx0*fac1+partx1*fac2;
            vv=partv0*fac1+partv1*fac2;
        end
        %-------------RK4 explicit scheme----------------
        function this=onestep_explicit_rk4(this,dt)
            %push particle using Runge-Kutta 4th order scheme
            %similar to Section E.1 of https://arxiv.org/pdf/1908.03824.pdf
            %or https://en.wikipedia.org/wiki/Runge%E2%80%93Kutta_methods
            dthalf=dt/2;
            %--calc. k1
            partx0=this.partx;
            partv0=this.partv;
            this=this.onestep_euler(dthalf);
            [xsum,vsum]=this.addxv(this.partx,this.partv,1,partx0,partv0,-1);
            %--calc. k2
            partx1=this.partx;
            partv1=this.partv;
            this=this.onestep_euler(dthalf);
            [dxtmp,dvtmp]=this.addxv(this.partx,this.partv,1,partx1,partv1,-1);
            [xsum,vsum]=this.addxv(xsum,vsum,1,dxtmp,dvtmp,2);
            %--calc. k3
            [this.partx,this.partv]=this.addxv(partx0,partv0,1,dxtmp,dvtmp,1);
            partx1=this.partx;
            partv1=this.partv;
            this=this.onestep_euler(dthalf);
            [dxtmp,dvtmp]=this.addxv(this.partx,this.partv,1,partx1,partv1,-1);
            [xsum,vsum]=this.addxv(xsum,vsum,1,dxtmp,dvtmp,2);
            %--calc. k4
            [this.partx,this.partv]=this.addxv(partx0,partv0,1,dxtmp,dvtmp,2);
            partx1=this.partx;
            partv1=this.partv;
            this=this.onestep_euler(dthalf);
            [dxtmp,dvtmp]=this.addxv(this.partx,this.partv,1,partx1,partv1,-1);
            [xsum,vsum]=this.addxv(xsum,vsum,1,dxtmp,dvtmp,1);
            
            dxvfac=1/6*2;
            [this.partx,this.partv]=this.addxv(partx0,partv0,1,xsum,vsum,dxvfac);
        end
        %-------------Euler scheme----------------
        function this=onestep_euler(this,dt)
            [Ex,Ey,dBdt] = 
            %--push particle using Euler scheme
% %             phik0=this.poisson_solve(this.partx);
% %             apark0=this.ampere_solve(this.partx,this.partv);
% %             
% %             var=this.particle_dxvdt_euler(this.partx,this.partv,phik0,apark0);
% %             
% %             this.partx=this.partx+dt*var.dxdt;
% %             this.partv=this.partv+dt*var.dvdt;
            %--save data--
            %   this.phik=phik0;
            %   this.apark=apark0;
        end
        
        function var=particle_dxvdt_euler(this,partx0,partv0,phik0,apark0)
            % right hand side of the particle's equations of motion (EOM) for
            % Euler scheme
            %--return var: dxdt,dvdt, i.e., right hand side of EOM
            partvphy=this.get_vphy(partx0,partv0,apark0);
            var.dxdt=partvphy;
            var.dvdt=2*real( (1i*this.kx*phik0 ...
                -1i*this.kx*apark0*partvphy).*exp(1i*this.kx*partx0));
        end
        
        function var=get_vphy(this,partx0,partv0,apark0)
            %calculate the physical velocity (var) using
            %partx0,partv0,apark0). vphy=partv+apar
            %--return var: vphy
            var=partv0+2*real( apark0*exp(1i*this.kx*partx0) );
        end
        
        
        function this=apply_bc(this)
            %--Particle B.C.
            this.partx=mod(this.partx,this.xwid)+this.xleft;
        end
        
        
        
        function this=test_em(this,dt,tend,ischeme_move)
            %  the test subroutine of the shear alfven wave
            %  Use: emnew.test_em(emnew.parms_aux.Ta/20,emnew.parms_aux.Ta)
            if ~exist('dt','var')||isempty(dt)
                dt=this.parms_aux.Ta/20;
            end
            if ~exist('tend','var')||isempty(tend)
                tend=this.parms_aux.Ta*0.5;
            end
            if ~exist('ischeme_move','var')||isempty(ischeme_move)
                ischeme_move=2;
            end
            
            disp('====this.test_em(dt,tend,ischeme_move,niter,npartiter)====')
            disp(['--Scheme: ',num2str(ischeme_move),' (1,2,3: Euler,rk4,implicit)--'])
            disp(['  ',num2str(floor(tend/dt)),' steps,dt,tend=',num2str(dt/this.parms_aux.Ta),',',num2str(tend/this.parms_aux.Ta)]);
            
            this.dt=dt;
            nrun=floor(tend/dt); % number of time steps, rounded down
            this.record_moments([],[],[],[],0);
            for fic=1:nrun
                this.record_moments(this.partx,this.partv,this.poisson_solve(this.partx),this.ampere_solve(this.partx,this.partv),fic);
                %--
                switch(ischeme_move)
                    case(1)
                        this=this.onestep_euler(dt);
                    case(2)
                        this=this.onestep_explicit_rk4(dt);
                end
                %--
                this=this.apply_bc;
            end
            this.data_energy=this.record_moments([],[],[],[],-1);
        end %--TEST_ES
        
        %--diagnostic session--
        function var=fitw_t1d(this,t1d,f1d,savefile)
            %--return var: fitted wave period, i.e., var.Tfig; fitted
            %frequency and damping/growth rate, i.e., var.omegafit,
            %var.gamma
            if ~exist('savefile','var');irec=0;savefile='';else;irec=1;end
            [fp1d,idxp1d]=findpeaks(abs(f1d));
            if(length(fp1d)>1)
                nperiod=(length(fp1d)-1)/2;
                var.Tfit=(t1d(idxp1d(end))-t1d(idxp1d(1)))/nperiod;
                var.omegafit=2*pi/var.Tfit;
                idxdiff=find(diff(t1d(idxp1d))<var.Tfit/2*0.2);
                disp(['----Too close peaks #=',num2str(length(idxdiff))]);
                
                logfabs1d=log(abs(f1d));
                pcoef=polyfit(t1d(idxp1d).',logfabs1d(idxp1d),1);
                logfabsfit1d=polyval(pcoef,t1d);
                
                fosc1d=f1d(:)./exp(logfabsfit1d(:));
                var.gamma=pcoef(1);
                disp(['fitted w=',num2str(var.omegafit+var.gamma*1i)])
                %--plot
                fid=figure('position',[10 50 600 280],'MenuBar','none');
                subplot(121)
                plot(t1d,real(fosc1d),'-',t1d,imag(fosc1d),'.--');grid;grid minor;
                xlabel('t');
                lid=legend('Re','Im');set(lid,'location','best','box','off');
                stit=['T_a=',num2str(this.parms_aux.Ta),',T_{fit}=',num2str(var.Tfit),', \omega/\omega_a=',num2str(var.omegafit/this.parms_aux.wa)];
                title(stit)
                
                subplot(122)
                plot(t1d,logfabs1d,'.-',t1d,logfabsfit1d,'--');grid;grid minor;
                hold on;plot(t1d(idxp1d),logfabs1d(idxp1d),'b+','markersize',16);
                xlabel('t');ylabel('ln|\delta\phi|');
                ylim([min(logfabs1d(:))/1.,max(logfabs1d(:))*1.0])
                title(['\gamma=',num2str(var.gamma),',\gamma/\omega_a=',num2str(var.gamma/this.parms_aux.wa)]);
                %--RECORD--
                if(irec==1)
                    savefile=[savefile,'.png'];
                    saveas(fid,savefile)
                end
            else
                var.gamma=NaN;
                var.omegafit=NaN;
            end
        end
        function var=p2fvpar(this,partv)
            %use the particle info to calculate the distribution along
            %velocity, i.e., f(v)
            %--returned value: var.v1d, var.fvpar are the distribution
            %function info, i.e., (var.v1d,var.fvpar) gives f(v), i.e.,
            %v_i=var.v1d; f_i=var.fvpar
            if ~exist('partvpar','var');partv=this.partv;end
            nv=80;
            fvenlarge=1;
            v1d=linspace(-this.vmaxovte*fvenlarge,this.vmaxovte*fvenlarge,nv);
            dv=v1d(2)-v1d(1);
            fpartvpar=min(v1d(end),max(v1d(1),partv));
            ifloor=discretize(fpartvpar,v1d);
            vv1=(fpartvpar-(dv*(ifloor-1)+v1d(1)))/dv;
            vv0=1-vv1;
            
            matp2g=sparse(ifloor,1:this.nptot,vv0,nv,this.nptot) ...
                +sparse(ifloor+1,1:this.nptot,vv1,nv,this.nptot);
            var.fvpar=sum(matp2g,2).'/dv/this.nptot;
            var.v1d=v1d;
            %figure;plot(fvpar);
        end
        function showdist(this,ifig)
            %--use this.showdist
            % visualize the gkem1d_cls object, e.g., distribution along v
            if ~exist('ifig','var')
                ifig=figure('position',[40 40 700 500]);
            else
                figure(ifig);
                set(ifig,'position',[40 40 700 500]);
            end
            figure(ifig);
            nbin=20;
            binwid=this.xwid/nbin;
            subplot(221)
            histogram(this.partx,'FaceColor',[0 0 1], ...
                'EdgeColor',[0 0 1],'Binwidth',binwid,'Normalization','pdf');
            xlabel('x');ylabel('f(x)');
            subplot(222)
            nvbin=40;
            binwid=2/nvbin;
            histogram(this.partv,'FaceColor',[0 0 1], ...
                'EdgeColor',[0 0 1],'Binwidth',binwid,'Normalization','pdf');
            hold on;
            xlabel('v/v_{tN}');ylabel('f(v)');
            nv=20;
            v1d=linspace(-2,2,nv);
            f_max=@(vv)1/sqrt(pi)*exp(-vv.^2);
            f1d=f_max(v1d);
            plot(v1d,f1d,'r--','linewidth',1.0);
            grid;grid minor;
            subplot(223)
            plot(this.partx,this.partv,'k.');xlabel('x');ylabel('v');
            subplot(224)
            fv=this.p2fvpar;
            plot(fv.v1d,(fv.fvpar-f_max(fv.v1d))/f_max(0),'k.-');
            xlabel('v/v_{tN}');ylabel('(f-f_M)/f_M(0)');grid;grid minor;
        end
        function data_energy=record_moments(this,partx,partv,phik,apark,irun,icase,sfile)
            if ~exist('sfile','var')
                sfile='moments_es.nc';
            end
            if ~exist('icase','var');icase=1;end
            if(irun==0)
                if exist(sfile,'file')==2;delete(sfile);end
                nccreate(sfile,'ektot','Dimensions',{'t',Inf});
                nccreate(sfile,'ephi','Dimensions',{'t',Inf});
                nccreate(sfile,'eapar','Dimensions',{'t',Inf});
            elseif(irun>0)
                partvphy=this.get_vphy(partx,partv,apark);
                ektot=0.5*sum(partvphy.^2)/this.nptot;
                ephi =0.5*this.kperp^2*real(phik*conj(phik))/this.Cpoisson;
                eapar=0.5*this.kperp^2*real(apark*conj(apark))/this.Campere;
                ncwrite(sfile,'ektot',ektot,irun);
                ncwrite(sfile,'ephi',ephi,irun);
                ncwrite(sfile,'eapar',eapar,irun);
            elseif(irun<0)
                ektot=ncread(sfile,'ektot');
                ektot=ektot-ektot(1);
                ephi=2*ncread(sfile,'ephi');
                eapar=2*ncread(sfile,'eapar');
                Etot=ektot+ephi+eapar;
                %relerrEtot=(Etot-mean(Etot))./Etot;
                relerrEtot=(Etot-Etot(1))/Etot(1);
                nrun=length(ektot);
                t1d=linspace(1,nrun,nrun)*this.dt;
                %--
                data_energy.ektot=ektot;
                data_energy.ephi=ephi;
                data_energy.eapar=eapar;
                
                x1d=t1d/this.parms_aux.Ta;
                if(icase==1)
                    figure('position',[640 40 350 400]);
                    %subplot(311)
                    subplot('position',[.15 .7 .8 .25])
                    plot(x1d,ektot,'k',x1d,ephi+eapar,'r-',x1d,Etot,'b-');
                    lid=legend('\int mv^2/2','E_E+E_B','E_{total}');
                    set(lid,'location','best','box','off');
                    ylabel('Energy');
                    ylim([min([ektot;Etot;ephi+eapar]) max([ektot;Etot;ephi+eapar])*1.05]);
                    set(gca,'XTickLabel','')
                    title(['\beta/M_e=',num2str(this.beta/this.me)])
                    grid;grid minor;
                    %subplot(312)
                    subplot('position',[.15 .4 .8 .25])
                    plot(x1d,ephi,'b-',x1d,eapar,'m-')
                    %hold on;plot(x1d,ephi+eapar,'r-');sleg={'E_E','E_B','E_E+E_B'};
                    sleg={'E_E','E_B'};
                    lid=legend(sleg);set(lid,'location','best','box','off');
                    ylabel('Energy');
                    set(gca,'XTickLabel','')
                    grid;grid minor;
                    %subplot(313)
                    subplot('position',[.15 .1 .8 .25])
                    plot(x1d,relerrEtot,'k');
                    ylabel('relerr E');
                    xlabel('t [T_{SAW}]');
                    %xlabel('t [1m/v_{te}]');
                    grid;grid minor;
                elseif(icase==2)
                    x1d=t1d/this.parms_aux.Ta;
                    figure('position',[40 340 350 300]);
                    subplot(211)
                    plot(x1d,ektot,'m-',x1d,ephi+eapar,'r-',x1d,Etot,'c-');
                    hold on;
                    plot(x1d,ephi,'k--',x1d,eapar,'b--');
                    ylabel('Energy');grid;
                    ylim([0 max(Etot)*1.5]);
                    %xlim([0,x1d(end)*1.25])
                    lid=legend('\int mv^2/2','E_E+E_B','E_{total}','E_E','E_B');set(lid,'location','northeast','box','off');
                    subplot(212)
                    plot(x1d,relerrEtot);
                    ylabel('Rel.err E');
                    xlabel('t [T_{SAW}]');
                    %xlim([0,x1d(end)*1.25])
                end
                %--Fit w&gamma
                ifit=1;
                if(ifit==1)
                    data_energy.result_fit=this.fitw_t1d(t1d,ephi);
                    disp(['  Fitting wave w=',num2str(data_energy.result_fit.omegafit+data_energy.result_fit.gamma*0.5i)]);
                end
            end
        end %RECORD_MOMENTS
    end
end
