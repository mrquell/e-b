clear; clc;

% Initialize parameters
kz = 2.5133;
N_m = 1e5; % Number of markers
s.N_PIF_modes = 500;
s.zleft = 0;
s.zwid = 2.5;
s.PIFfac = 500;
s.Density = zeros(1,s.N_PIF_modes*s.PIFfac);

% Initialize particles
condition = 3;
if condition == 1 %Uniform
    z = linspace(s.zleft,s.zleft+s.zwid,N_m);
elseif condition == 2 % Uniform with asymetric perturbation
    z = linspace(s.zleft,s.zleft+s.zwid,N_m); 
    z = z+0.01*cos(4.*pi./s.zwid.*z); 
    histogram(z)
elseif condition == 3% Random 
    z = rand(1,N_m).*s.zwid+s.zleft; 
elseif condition == 4 % Random with asymetric perturbation
    z = rand(1,N_m).*s.zwid+s.zleft;
    z = z+0.01*cos(4.*pi./s.zwid.*z);
end

% Apply periodic BC
z(z>s.zleft+s.zwid) = z(z>s.zleft+s.zwid)-s.zwid;
z(z<s.zleft) = z(z<s.zleft)+s.zwid;

% Calculate density
s.cor(1,:) = z;
s = calc_Density(s);


% Plot results
% close all
% figure(1)
% plot(y,'b.')
% ylim([y_left, y_left+y_width])
% figure(2)
% plot(y_rep,y_cors)
% ylim([y_left, y_left+y_width])
figure(3)
clf
hold on
histogram(z,s.N_PIF_modes,'FaceColor','blue')
xlim([s.zleft, s.zleft+s.zwid])
%figure(4)
fac = s.N_PIF_modes/s.zwid;
z_cors = linspace(s.zleft,s.zleft+s.zwid,s.N_PIF_modes*s.PIFfac);
plot(z_cors,s.Density/fac,'red')
xlim([s.zleft, s.zleft+s.zwid])
xlabel('z')
ylabel('number density / factor')
hold off

% Direct copy from fluid_cls.m
function s = calc_Density(s)
            z = s.cor(1,:);
            k_vec = 2*pi./s.zwid .* (0:s.N_PIF_modes-1);
           
            % Calculate Fourrier components
            k_rep1 = repmat(k_vec,length(z),1); 
            y_rep = repmat(z',1,s.N_PIF_modes);
            n_k = 1/s.zwid * sum(exp(-1i.*k_rep1.*y_rep),1);
            
            % Compute density in configuration space
            z_cors = linspace(s.zleft,s.zleft+s.zwid,s.N_PIF_modes*s.PIFfac);
            k_rep2 = repmat(k_vec,s.N_PIF_modes*s.PIFfac,1);
            z_cors_rep = repmat(z_cors',1,s.N_PIF_modes);
            s.Density = 2*real(sum( n_k .* exp(1i.*k_rep2.*z_cors_rep),2))-real(n_k(1));
end