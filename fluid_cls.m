%=======================A pure fluid test code=======================
% memo:
% 2021/2/3: simple 2 equations by ZLU
%
% Equations:
%   dEx/dt= Bx  (1)
%   dBx/dt=-Ex  (2)
% analytical dispersion relation: d^2 E/dt^2+E=0, i.e., w^2=1

classdef fluid_cls
    properties
        dt
        Exk
        Exk_bar
        Byk
        kx
        kz
        cor
        Density
        PIFfac
        parms_aux
        sfile
        xwid
        ywid
        zwid
        xleft
        yleft
        zleft
        N_mark
        N_particles
        N_PIF_modes
    end % PROPERTIES
    methods
        function s = fluid_cls
            %--set initial values
            s.Exk = 1e-8;
            s.Byk = 1e-1;
            
            %--calculate auxiliary parameters
            s.N_mark = 10e3;
            s.N_particles = 10e3;
            s.N_PIF_modes = 3;
            s.PIFfac = 5;
            s.Density = zeros(1,s.N_PIF_modes);
            s.parms_aux.B0 = 0.5;
            s.parms_aux.omega_A = 1;
            
            s.parms_aux.beta = 0.01;
            s.parms_aux.miome = 1836;
            s.parms_aux.rho_i = .01;%s.parms_aux.B0^2/(4*pi*s.parms_aux.omega_A^2); %ERROR. THIS IS RHO_M NOT RHO_I. Frequency does not depend on this though...
            s.parms_aux.c = 29979245800; %cm/s, cgs
            s.parms_aux.e = 4.8e-10; % charge in cgs
            s.parms_aux.me = 9.10938370e-28; %g
            s.parms_aux.v_thermal = 1e5; % cm/s
            s.parms_aux.R_N = 100; %cm
            %--find wave numbers
            s.kx = 1/s.parms_aux.rho_i * 0.1;
            s.kz = s.kx/10;
            s.Exk_bar = s.Exk * s.parms_aux.c;
            
            
            s.zleft = 0;
            s.zwid = 2*pi/s.kz;
            
            s.cor(1,:) = rand(s.N_mark,1).*s.zwid+s.zleft;
%             pertz = 0.005;
%             s.cor(1,:) = s.cor(1,:)+ pertz.*sin(s.kz*s.cor(1,:));
%             s.cor(2,:) = zeros(1,s.N_mark);
            s.cor(2,:) = normrnd(0,1/sqrt(2),s.N_mark,1);
        end
        
        function s = test_fluid(s,tend,dt)
            if~exist('tend','var');tend = 3*pi*6;end
            if~exist('dt','var');dt = 0.05;end
            nrun = floor(tend/dt);
            s.dt = dt;
            s = s.record([],[],0); %saving
            for fic = 1:nrun
                s = s.onestep_rk4(dt); %compute the next field & particles
                s.record(s.Exk_bar,s.Byk,fic,s.sfile);%save result
                s = s.setBC;
%                 s = s.calc_Density;
%                 s.plotstep(fic)
            end
            s.plot_end; %plot the end results
        end
        
        function s = record(s,Exk_bar,Byk,irun,sfile)
            if ~exist('sfile','var')
                sfile = 'data_field.nc';
                s.sfile = sfile;
            end
            if(irun == 0)
                if exist(sfile,'file') == 2
                    delete(sfile);
                end
                nccreate(sfile,'Exkb','Dimensions',{'t',Inf});
                nccreate(sfile,'Byk','Dimensions',{'t',Inf});
                nccreate(sfile,'dt');
                ncwrite(sfile,'dt',s.dt);
            elseif(irun > 0)
                ncwrite(sfile,'Exkb',real(Exk_bar),irun)
                ncwrite(sfile,'Byk',real(Byk),irun)
            end
        end
        
        function s = onestep_euler(s,dt)
            var = s.get_dfdt(s.Exk_bar, s.Byk);
            s.Exk_bar = s.Exk_bar + var.dExk_bar_dt * dt;
            s.Byk = s.Byk + var.dBykdt * dt;
            s.cor(1,:) = s.cor(1,:) + s.cor(2,:) * dt;
            s.cor(2,:) = s.cor(2,:) + var.dvzdt .* dt;
            
        end
        
        function var=get_dfdt(s,Exk_bar,Byk)
            p = s.parms_aux;
            var.dExk_bar_dt = -1i .* p.omega_A^2 .* s.kz .* Byk .* (1 - 3/8 * (s.kx*p.rho_i).^2).^-1;
            var.dBykdt = -1i .* s.kz .* Exk_bar .* (1 + (1 + p.miome .* p.beta ./ (s.kx.*p.rho_i).^2).^-1);
            Ez = s.kz./s.kx .* Exk_bar .* 1./(1+p.miome*p.beta./(s.kx*p.rho_i).^2);
            var.dvzdt = -p.e/p.me * (p.R_N/ p.v_thermal^2) * real(2 * Ez * exp(1i*s.kz*s.cor(1,:)));
        end
        
        function s = onestep_rk4(s,dt)
            dthalf = dt/2;
            E = s.Exk_bar;
            B = s.Byk;
            % Calculate k1
            E1 = E;
            B1 = B;
            k1 = s.get_dfdt(E1,B1);
            % Calculate k2
            E2 = E + dthalf * k1.dExk_bar_dt;
            B2 = B + dthalf * k1.dBykdt;
            k2 = s.get_dfdt(E2,B2);
            % Calculate k3
            E3 = E + dthalf * k2.dExk_bar_dt;
            B3 = B + dthalf * k2.dBykdt;
            k3 = s.get_dfdt(E3,B3);
            % Calculate k4
            E4 = E + dt * k3.dExk_bar_dt;
            B4 = B + dt * k3.dBykdt;
            k4 = s.get_dfdt(E4,B4);
            % Determine next position
            s.Exk_bar = s.Exk_bar + dthalf/3 * (k1.dExk_bar_dt + 2*k2.dExk_bar_dt + 2*k3.dExk_bar_dt + k4.dExk_bar_dt);
            s.Byk = s.Byk + dthalf/3 * (k1.dBykdt + 2*k2.dBykdt + 2*k3.dBykdt + k4.dBykdt);
        end
        
        function s = setBC(s)
            wavelength = 2*pi/s.kz;
            s.cor(1,s.cor(1,:) > wavelength) = s.cor(1,s.cor(1,:)> wavelength) - wavelength;
            s.cor(1,s.cor(1,:) < 0) = s.cor(1,s.cor(1,:) < 0) + wavelength;
        end
        
        function s = calc_Density(s)
            z = s.cor(1,:);
            k_vec = 2*pi./s.zwid .* (0:s.N_PIF_modes-1);
           
            % Calculate Fourrier components
            k_rep1 = repmat(k_vec,length(z),1); 
            y_rep = repmat(z',1,s.N_PIF_modes);
            n_k = 1/s.zwid * sum(exp(-1i.*k_rep1.*y_rep),1);
            
            % Compute density in configuration space
            z_cors = linspace(s.zleft,s.zleft+s.zwid,s.N_PIF_modes*s.PIFfac);
            k_rep2 = repmat(k_vec,s.N_PIF_modes*s.PIFfac,1);
            z_cors_rep = repmat(z_cors',1,s.N_PIF_modes);
            s.Density = 2*real(sum( n_k .* exp(1i.*k_rep2.*z_cors_rep),2))-real(n_k(1));
        end
        
        function plotstep(s,fic)
            figure(1)
            clf;
            hold on
            subplot(1,2,1)
            y = (real(s.cor(1,:)));
            plot(y,'b.')
            ylabel('z')
            xlabel('Particle index')
            ylim([0,2*pi/s.kz])
            title(['Particles at step ',num2str(fic)])
            
            subplot(1,2,2)
            x = s.Density;
            y = linspace(s.zleft,s.zleft+s.zwid,s.N_PIF_modes*s.PIFfac);
            plot(x,y,'r')
            ylim([0,2*pi/s.kz])
            title('Density')
            xlabel('Number density')
            ylabel('z')
            pause(0.000001)
            hold off
            
            figure(2)
            clf;
            x = real(s.cor(1,:));
            y = real(s.cor(2,:));
            plot(x,y,'b.')
            xlabel('z')
            ylabel('v_z')
            title('Dependence of v_z on z')
           % ylim([-.1,.1])
        end
        
        function var = plot_end(s)
            var.Exk = ncread(s.sfile,'Exkb')*1e-3;
            var.Byk = ncread(s.sfile,'Byk');
            var.dt = ncread(s.sfile,'dt');
            
            nrun = length(var.Exk);
            t1d =(1:nrun) * s.dt;
            
            T_Exk = s.calc_period(var.Exk,t1d);
            omega_Exk = 2*pi / T_Exk;
            T_Byk = s.calc_period(var.Byk,t1d);
            omega_Byk = 2*pi / T_Byk;
            
            omega = mean([omega_Exk omega_Byk]);
            disp('Compare')
            disp('Numerical')
            disp('omega / omega_A /k_z')
            disp((omega/s.parms_aux.omega_A/s.kz))
            
            fdisp = @(x) sqrt( (1 + 1./(1+1./(x.^2/s.parms_aux.beta/s.parms_aux.miome))) ./ (1- 3/8*x.^2) );
            omega_anal = fdisp(s.kx*s.parms_aux.rho_i);
            disp('Analytic')
            disp(omega_anal)
            
            figure('position',[50 50 500 400])
            plot(t1d,var.Exk,'r',t1d,var.Byk,'k')
            lid = legend(strcat('Ex, T=',sprintf('%2.3f',T_Exk)),strcat('By, T=',sprintf('%2.3f',T_Byk)));
            set(lid,'location','best','box','off')
            %title(['theory T=', num2str(s.parms_aux.T)])
            xlabel('Time, s')
            ylabel('Field strength')
            grid;
            grid minor;
        end
        
        function T = calc_period(~,y,x)
            try
                [pk,locs] = findpeaks(abs(y),x);
                [~,locs_diff] = findpeaks(abs(diff(y)),x(1:end-1));
                per = mean(diff(sort([0,locs, locs_diff])))*4; % Estimate period from zeros and peaks
                if isnan(per)
                    per = max(x)*4;
                    disp('Error: not enough oscilations')
                end
                if (y(1)>(y(2)-y(1)))
                    pk = [y(1);pk];
                end
                decay = pk(end) * log(pk(end)/pk(1));
                amp = (pk(1)+pk(2))/2;
                offset = 0;                               % Estimate offset
                fit = @(b,x)  b(1).*exp(b(5)*x).*(sin(2*pi*x./b(2) + b(3))) + b(4);    % Function to fit
                options = optimoptions('lsqcurvefit','OptimalityTolerance',1e-30,'FunctionTolerance',1e-30,'Display','off');
                sval = lsqcurvefit(fit,[amp,per,pi/2,offset,decay],x,y',[],[],options);
                T = sval(2);
                xp = linspace(min(x),max(x)); figure(); plot(x,y,'b.-',  xp,fit(sval,xp), 'r-')
                grid; title('Least squares fit function'); legend('data','fit');
            catch
                T = inf;
            end
        end
    end
end












